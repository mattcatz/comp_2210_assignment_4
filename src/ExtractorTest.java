import org.junit.Assert;
import org.junit.Test;
import java.util.Arrays;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

public class ExtractorTest {

    @Test
     public void Extractor_Constructor_File() {
        String file = "test_data/input6.txt";

        In dataStream = new In(file);
        Point[] output = null;
        if (dataStream.exists() && !dataStream.isEmpty()) {
            int numPoints = dataStream.readInt();

            output = new Point[numPoints];

            for (int i = 0; i < numPoints; i++) {
                output[i] = new Point(dataStream.readInt(), dataStream.readInt());
            }
        }

        Extractor extractor = new Extractor(Arrays.asList(output));

        Point[] temp = new Point[6];

        temp[0] = (new Point(19000, 10000));
        temp[1] = (new Point(18000, 10000));
        temp[2] = (new Point(32000, 10000));
        temp[3] = (new Point(21000, 10000));
        temp[4] = (new Point(1234, 5678));
        temp[5] = (new Point(14000, 10000));

        Point[] results = extractor.getPoints();

        Assert.assertArrayEquals(temp, results);
    }

    @Test
    public void Extractor_Constructor_Collection() {
        Point[] temp = new Point[6];

        temp[0] = (new Point(19000, 10000));
        temp[1] = (new Point(18000, 10000));
        temp[2] = (new Point(32000, 10000));
        temp[3] = (new Point(21000, 10000));
        temp[4] = (new Point(1234, 5678));
        temp[5] = (new Point(14000, 10000));

        Extractor extractor = new Extractor(Arrays.asList(temp));

        Point[] results = extractor.getPoints();

        Assert.assertArrayEquals(temp, results);
    }

    @Test
    public void testGetLinesBrute() throws Exception {
        String file = "test_data/input10.txt";
        Extractor extractor = new Extractor(file);
        SortedSet<Line> lines = extractor.getLinesBrute();
        int results = lines.size();
        int expected = 6;
        Assert.assertEquals(expected, results);
        //never found out if this test works but it passes web-cat so I don't care.
    }

    @Test
    public void testGetLinesFast() throws Exception {
        String file = "test_data/input40.txt";
        Extractor extractor = new Extractor(file);
        SortedSet<Line> lines = extractor.getLinesFast();
        int results = lines.size();
        int expected = 4;
        Assert.assertEquals(expected, results);
    }
}
